# sig_stack

Software stack for running Special Interest Groups

**TODO**: Start with a list of the free software which can be used to organize and run a local group. Including physical meetups and virtual meetups. Eventually, this should include configuration to make this as easy as possible.

* chat
  - [Matrix](https://matrix.org/)
* video chat
  - [Jitsi](https://jitsi.org/)
  - [Big Blue Button](https://bigbluebutton.org/)
* video recording
  - [OBS](https://obsproject.com/)
* video distribution
  - [PeerTube](https://peer.tube/)
  - YouTube
  - Vimeo
  - Internet Archive
* audio distribution
  - RSS/static hosted
  - Apple Podcasts
  - Spotify
  - Google Podcasts
  - Overcast
  - Sticher
* shared document editing
  - [Etherpad](https://etherpad.org/)
* calendar and scheduling
  - [Meetup](https://www.meetup.com/)(proprietary)
* website
  - various static HTML generators (e.g., middleman)
  - [Drupal](https://www.drupal.org/)
  - [Wordpress](https://wordpress.com/)
* mailing list or forum
  - [mailman](http://www.list.org/)
  - [Discourse](https://www.discourse.org/)
* social media management and promotion


# Reference material

* https://opensource.com/article/20/5/free-software-communication
* https://opensource.com/article/20/5/open-source-video-conferencing
* https://www.fsf.org/news/free-software-foundation-announces-freedom-respecting-videoconferencing-for-its-associate-members
* https://opensource.com/article/20/5/conference-free-software
